/* jshint esnext: true */
'use strict';

var dirty = require('dirty');
var mifalim=dirty('mifalim.db');
mifalim.on('load', function() {
  mifalim.forEach( (id, mifal) => {
    mifal.nextRunDate = '201507';
    mifal.completed = false;
    mifalim.set(id, mifal);
  } );
});
