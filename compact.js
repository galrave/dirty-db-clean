/* jshint esnext:true*/
var dirty = require('dirty');
var mifalimDb=dirty('mifalim.db');
mifalimDb.on('load', function() {
  var recordMap = {};
  mifalimDb.forEach( (key, val) => val&&(recordMap[key]=val) );

  var cnt =0;
  var stream = require('fs').createWriteStream('mifalim.json');
  for (var key in recordMap) {
    stream.write(JSON.stringify({
      key: key,
      val: recordMap[key]
    }));

    stream.write('\n');
    cnt++;
  }

  stream.end();
  console.log('Number of records: ' + cnt);
});
